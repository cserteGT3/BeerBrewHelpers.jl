@testset "SpecificGravity constructor" begin
    sg1 = SpecificGravity(1)
    @test isapprox(sg1.val, 1)
    sg1 = SpecificGravity(1.120)
    @test isapprox(sg1.val, 1.120)
    sg1 = SpecificGravity(1.049)
    @test isapprox(sg1.val, 1.049)
    
    sg1 = SpecificGravity(1000)
    @test isapprox(sg1.val, 1)
    sg1 = SpecificGravity(1120)
    @test isapprox(sg1.val, 1.120)
    sg1 = SpecificGravity(1049)
    @test isapprox(sg1.val, 1.049)
    
    @test_throws DomainError SpecificGravity(0)
    @test_throws DomainError SpecificGravity(2)
    @test_throws DomainError SpecificGravity(900)
    @test_throws DomainError SpecificGravity(2000)

    @test SG(1) == SG(1000)
    @test SG(1.200) == SG(1200)

    @test isapprox(SG(1), SG(1000))
    @test isapprox(SG(1.1200), SG(1120))
end

@testset "SG printing" begin
    SGS = BeerBrewHelpers.SpecificGravitystring

    @test BeerBrewHelpers.PRINTMODE[] == 0

    @test SGS(SG(1000)) == "SG 1.0000"
    @test SGS(SG(1.2345)) == "SG 1.2345"
    @test SGS(SG(1.00001)) == "SG 1.0000"

    @test setprintmode(1) === nothing
    @test BeerBrewHelpers.PRINTMODE[] == 1

    @test SGS(SG(1000)) == "SG 1000"
    @test SGS(SG(1.2345)) == "SG 1234"
    @test SGS(SG(1.00001)) == "SG 1000"

    @test_throws DomainError setprintmode(-1)
    @test_throws DomainError setprintmode(2)

    @test setprintmode(0) === nothing
    @test BeerBrewHelpers.PRINTMODE[] == 0
end

@testset "conversions" begin
    @test isapprox(sg2plato(SpecificGravity(1002)), 0.5; atol=0.15)
    @test isapprox(sg2plato(SpecificGravity(1.036)), 9; atol=0.15)
    @test isapprox(sg2plato(SpecificGravity(1.057)), 14; atol=0.15)
    @test isapprox(sg2plato(SpecificGravity(1.083)), 20; atol=0.15)
    @test isapprox(sg2plato(SpecificGravity(1134)), 31; atol=0.15)
    @test isapprox(sg2plato(SpecificGravity(1179)), 40; atol=0.15)

    @test isapprox(plato2sg(0.5).val, 1.002, atol=0.0005)
    @test isapprox(plato2sg(9).val, 1.036, atol=0.0005)
    @test isapprox(plato2sg(14).val, 1.057, atol=0.0005)
    @test isapprox(plato2sg(20).val, 1.083, atol=0.0005)
    @test isapprox(plato2sg(31).val, 1.134, atol=0.0005)
    @test isapprox(plato2sg(40).val, 1.179, atol=0.0005)
end

@testset "gravity correction" begin
    @test_throws DomainError gravitycorrection(SG(1000), SG(1100), 1)
    @test_throws DomainError gravitycorrection(SG(1100), SG(1000), 1)


    @test isapprox(BeerBrewHelpers.boilofftime(1.5, 2.5).time, 1.6666; atol=0.0017)
    @test isapprox(BeerBrewHelpers.boilofftime(2.5, 1.5).time, 0.6; atol=0.0017)
    @test_throws DomainError BeerBrewHelpers.boilofftime(0, 2.5)

    @test isapprox(gravitycorrection(SG(1050), SG(1025), 1), 1; atol=0.01)
    @test isapprox(gravitycorrection(SG(1105), SG(1079), 13.7), 4.51; atol=0.01)
    @test isapprox(gravitycorrection(SG(1050), SG(1075), 1), -0.333; atol=0.01)
    @test isapprox(gravitycorrection(SG(1079), SG(1105), 13.7), -3.39; atol=0.01)
    @test isapprox(gravitycorrection(SG(1079), SG(1079), 13.7), 0; atol=0.01)
end

@testset "mixing wort" begin
    mix1 = mixwort(SG(1050), 1, SG(1), 1)
    mix2 = mixwort(SG(1050), 1, SG(1.1), 1)
    mix3 = mixwort(SG(1080), 10, SG(1.2), 5)

    @test isapprox(mix1.sg.val, 1.025)
    @test mix1.vol == 2
    @test isapprox(mix2.sg.val, 1.075)
    @test mix2.vol == 2
    @test isapprox(mix3.sg.val, 1.12)
    @test mix3.vol == 15
end

@testset "grist composition" begin
    @test isnothing(gristratio([7700, 1200]))
end

@testset "measure diluted wort" begin
    @test isapprox(mdwort(SG(1), 1, 1), SG(1))
    @test isapprox(mdwort(SG(1), 2, 1), SG(1))
    @test isapprox(mdwort(SG(1), 1, 3.5), SG(1))

    @test isapprox(mdwort(SG(1060), 1, 1), SG(1120))
    @test isapprox(mdwort(SG(1120), 1, 1), SG(1240))

    @test isapprox(mdwort(SG(1120), 1, 2), SG(1360))
    @test isapprox(mdwort(SG(1080), 1, 2), SG(1240))
    @test isapprox(mdwort(SG(1100), 1, 3), SG(1400))
end

@testset "kettle volume calculation" begin
    BBHm2m = BeerBrewHelpers.measurement2metric

    @test isapprox(BBHm2m(1, :m), 1)
    @test isapprox(BBHm2m(10, :dm), 1)
    @test isapprox(BBHm2m(100, :cm), 1)
    @test isapprox(BBHm2m(1000, :mm), 1)
    @test_throws DomainError BBHm2m(10, :oz)

    kettle_diam = 2.5 #dm
    kettle_height = 3 #dm
    m_height = 2 #dm
    # wort volume should be 4,90
    @test isapprox(kettlevol(kettle_diam, kettle_height, m_height, unit=:dm), 0.004908; atol=0.000001)
    @test isapprox(kettlevol(kettle_diam, kettle_height, m_height, unit=:m), 4.908738; atol=0.000001)
end

@testset "info()" begin
    @test info() === nothing
end

@testset "whacky sugar calculations" begin
    # this is in gramms, so not much tolerance is needed
    @test isapprox(sgvol2sugar(SG(1384), 1), 1000; atol = 0.01)
    @test isapprox(sgvol2sugar(SG(1190), 0.2), 98.95; atol = 0.01)

    @test isapprox(dme2sugar(1), 44/46)
    # this is in gramms, so not much tolerance is needed
    @test isapprox(dme2sugar(104), 99.47; atol = 0.01)

    # this is in gramms, so not much tolerance is needed
    @test isapprox(raisesgbysugar(384, 1), 1000)
    @test isapprox(raisesgbysugar(192, 1), 500)
    @test isapprox(raisesgbysugar(384, 0.5), 500)

    @test isapprox(sgofsugarliq(1000, 1), SG(1384))
    @test isapprox(sgofsugarliq(500, 1), SG(1192))
    @test isapprox(sgofsugarliq(1000, 0.5), SG(1768))
end

@testset "total gravity calculation" begin
    @test totalsg(SG(1000), 1, 1000) == SG(1000+BeerBrewHelpers.SUCROSEPKL)
    @test totalsg(SG(1000), 1, 0) == SG(1)
end
