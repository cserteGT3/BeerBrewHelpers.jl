@testset "parse logfile" begin
    dates = [DateTime(2020,12,19,19,04,00),
    DateTime(2020,12,19,19,32,36),
    DateTime(2020,12,19,20,01,14),
    DateTime(2020,12,19,20,29,51),
    DateTime(2020,12,19,20,58,30),
    DateTime(2020,12,20,05,33,11),
    DateTime(2020,12,20,06,01,49),
    DateTime(2020,12,26,00,20,43),
    DateTime(2020,12,26,00,49,23),
    DateTime(2020,12,26,01,17,59),
    DateTime(2022,3,5,14,29,23),
    DateTime(2022,3,5,15,12,14),
    DateTime(2022,3,5,16,9,29),
    DateTime(2022,3,5,16,23,48),
    DateTime(2022,3,5,16,38,14),
    DateTime(2022,3,5,16,52,40)]

    angles = [45.73367,
    45.71967,
    45.73301,
    45.69114,
    45.6977,
    45.32423,
    45.31193,
    27.20456,
    27.19978,
    27.17694,
    26.54017,
    26.52005,
    25.55783,
    39.23592,
    83.35023,
    84.66725]

    sgs = [1.0475,
    1.0475,
    1.0475,
    1.0474,
    1.0475,
    1.0467,
    1.0467,
    1.0099,
    1.0099,
    1.0099,
    1.0086,
    1.0086,
    1.0066,
    1.0345,
    1.1195,
    1.1219]

    temps = [22.06,
    21.93,
    22.0,
    21.93,
    21.87,
    21.75,
    21.75,
    20.68,
    20.68,
    20.68,
    5.75,
    5.875,
    6.0625,
    6.25,
    8.4375,
    10.375]

    logfile = processlogfile(joinpath(@__DIR__, "ispindel_test.log"))

    @testset "dates" begin
        for (i,d) in enumerate(logfile.date)
            @test dates[i] == d
        end
    end
    @testset "angles" begin
        for (i,a) in enumerate(logfile.angle)
            @test isapprox(angles[i], a; atol=0.000001)
        end
    end
    @testset "SGs" begin
        for (i,sg) in enumerate(logfile.sg)
            @test isapprox(sgs[i], sg; atol=0.0001)
        end
    end
    @testset "temperature" begin
        for (i,t) in enumerate(logfile.temperature)
            @test isapprox(temps[i], t; atol=0.01)
        end
    end
end

@testset "parse tuya-inkbird log file" begin
    dates = [DateTime(2022,4,13,16,39,37),
    DateTime(2022,4,13,16,40,27),
    DateTime(2022,4,13,22,55,0)]

    temps = [22.8,
    22.8,
    23.0]

    set_temps = [23,
    22.6,
    22.6]

    logfile = processtuyalogfile(joinpath(@__DIR__, "tuya-inkbird-test.log"))

    @testset "dates" begin
        for (i,d) in enumerate(logfile.date)
            @test dates[i] == d
        end
    end
    @testset "temperatures" begin
        for (i,t) in enumerate(logfile.temperature)
            @test isapprox(temps[i], t; atol=0.01)
        end
    end
    @testset "set temperatures" begin
        for (i,st) in enumerate(logfile.set_temperature)
            @test isapprox(set_temps[i], st; atol=0.01)
        end
    end
end
