using Documenter, BeerBrewHelpers

makedocs(
    modules = [BeerBrewHelpers],
    format = Documenter.HTML(; prettyurls = get(ENV, "CI", nothing) == "true"),
    authors = "Tamás Cserteg",
    sitename = "BeerBrewHelpers.jl",
    pages = Any["index.md"]
    # strict = true,
    # clean = true,
    # checkdocs = :exports,
)
