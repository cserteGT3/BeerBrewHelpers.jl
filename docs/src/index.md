# BeerBrewHelpers

## API - Planning tools

```@docs
SpecificGravity
setprintmode
plato2sg
sg2plato
gravitycorrection
mixwort
gristratio
mdwort
kettlevol
dme2sugar
sgvol2sugar
raisesgbysugar
sgofsugarliq
info
```

## API - iSpindel log process

These functions process logs from ispindel-tracked fermentations.

```@docs
processlogfile
processtuyalogfile
masg
promasg
```
