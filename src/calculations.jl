"""
    SpecificGravity

Store specific gravity in its scientific form (e.g 1.050).
Can be constructed either by `SpecificGravity(1.050)` or `SpecificGravity(1050)`.

The alias `SG` can be used.

# Examples
```julia-repl
julia> SpecificGravity(1050)
SG 1050

julia> SpecificGravity(1.050)
SG 1050

julia> SG(1.050)
SG 1050
```
"""
struct SpecificGravity
    val::Float64
    function SpecificGravity(value)
        if 0 < value && value < 2
            return new(value)
        elseif 900 < value && value < 2000
            return new(value/1000)
        else
            throw(DomainError(value, "SpecificGravity is not defined for such number"))
        end
    end
end

Base.isapprox(x1::SpecificGravity, x2::SpecificGravity; args...) = isapprox(x1.val, x2.val; args...)

const SG = SpecificGravity

"""Global variable to store printing mode."""
const PRINTMODE = Ref(0)

"""Sucrose point per kg per liter from BrewFather."""
const SUCROSEPKL = 384
"""Sucrose point per pound per (US) gallon from BrewFather."""
const SUCROSEPPG = 46

"""
   setprintmode(mode)

Set the printing of `SG` to either "SG 1000" or "SG 1.000" form.
See Examples for details.
Default is 0.

# Examples

```julia-repl
julia> SG(1234)
SG 1.2340

julia> setprintmode(0)
Print mode is set to SG 1.0000

julia> SG(1234)
SG 1.2340

julia> setprintmode(1)
Print mode is set to SG 1000

julia> SG(1234)
SG 1234
```
"""
function setprintmode(mode)
    (mode != 0 && mode != 1) && throw(DomainError(mode, "Usable print modes: 0 and 1. Printmode hasn't been changed."))
    PRINTMODE[] = mode
    println("Print mode is set to $(SpecificGravitystring(SG(1000)))")
end

function SpecificGravitystring(g::SG)
    PRINTMODE[] == 0 && return "SG $(@sprintf("%.4f", g.val))"   
    PRINTMODE[] == 1 && return "SG $(round(Int, 1000*g.val))"
end

Base.show(io::IO, g::SG) = print(io, SpecificGravitystring(g))

## specific gravity conversion
# source: https://www.brewersfriend.com/plato-to-sg-conversion-chart/

"""
    plato2sg(plato)

Convert Plato° to specific gravity.

# Examples
```julia-repl
julia> plato2sg(12)
SG 1048
```
"""
function plato2sg(plato)
    sg = 1+(plato/(258.6-((plato/258.2)*227.1)))
    return SpecificGravity(sg)
end

"""
    sg2plato(sg::Number)

Convert specific gravity to Plato°.
This method accepts numbers.

# Examples
```julia-repl
julia> sg2plato(1.050)
12.38764712500003
```
"""
sg2plato(sg::Number) = (-1*616.868)+(1111.14*sg)-(630.272*sg^2)+(135.997*sg^3)

"""
    sg2plato(sg::SG)

A method for converting specific gravity to Plato°, that accepts `SpecificGravity`.

# Examples
```julia-repl
julia> sg2plato(SG(1050))
12.38764712500003
```
"""
sg2plato(sg::SG) = sg2plato(sg.val)

## gravity correction: boil-off or dilute

"""
    boilofftime(boiloffrate, boiloffvolume)

Calculate the boil time based on the rate and the to-be-boiled-off volume.
Also construct a nice message.

# Examples

```julia-repl
julia> boilofftime(1.5, 2.5)
(time = 1.6666666666666667, message = " With 1.5L/h: 1.67 hours (100 minutes).")
```
"""
function boilofftime(boiloffrate, boiloffvolume)
    boiloffrate <= 0 && throw(DomainError(boiloffrate, "Boiloffrate can't be zero or less."))
    hours = boiloffvolume/boiloffrate
    m = round(Int, hours*60)
    msg = " With $(boiloffrate)L/h: $(@sprintf("%.2f", hours)) hours ($m minutes)."
    return (time = hours, message = msg)
end

"""
    gravitycorrection(currentg::SG, targetg::SG, currentvol; boiloffr=nothing)

Calculate the needed correction to achieve `targetg` gravity.

If `boiloffr` boil-off rate is given, and wort should be boiled off, the needed boil time is also calculated.

# Arguments
- `currentg::SG`: current specific gravity.
- `targetg::SG`: target specific gravity.
- `currentvol`: current volume in L.
- `boiloffr=nothing`: boil-off rate in L/h (or `nothing`).

# Examples

```julia-repl
julia> gravitycorrection(SG(1050), SG(1025), 1)
Dilute with 1.00L water.
1.0000000000000089

julia> gravitycorrection(SG(1050), SG(1075), 1)
Boil off 0.33L water.
-0.33333333333333237

julia> gravitycorrection(SG(1050), SG(1075), 1, boiloffr=2.5)
Boil off 0.33L water. With 2.5L/h: 0.13 hours (8 minutes).
-0.33333333333333237
```
"""
function gravitycorrection(currentg::SG, targetg::SG, currentvol; boiloffr=nothing)
    isapprox(currentg.val, 1, atol = 0.0005) && throw(DomainError(currentg, "Can't do anything with water density wort!"))
    isapprox(targetg.val, 1, atol = 0.0005) && throw(DomainError(targetg, "Can't dilute to water density or lower!"))
    correction_vol = (currentg.val-targetg.val)*currentvol/(targetg.val-1)
    if correction_vol > 0
        println("Dilute with $(@sprintf("%.2f", correction_vol))L water.")
    elseif correction_vol < 0
        corr_vol = abs(correction_vol)
        boil_time_msg = isnothing(boiloffr) ? "" : boilofftime(boiloffr, corr_vol).message
        println("Boil off $(@sprintf("%.2f", corr_vol))L water.", boil_time_msg)
    else
        println("You're right at target gravity! ;)")
    end
    return correction_vol
end

## mix wort

"""
    mixwort(sg1::SG, v1, sg2::SG, v2)

Calculate the resulting wort's gravity when mixing the given the liquids.

# Arguments
- `sg1::SG`: specific gravity of liquid 1.
- `v1`: volume (in L) of liquid 1.
- `sg2::SG`: specific gravity of liquid 2.
- `v2`: volume (in L) of liquid 2.

# Examples

```julia-repl
julia> mixwort(SG(1050), 1, SG(1000), 1)
(sg = SG 1025, vol = 2)

julia> mixwort(SG(1050), 1, SG(1100), 1)
(sg = SG 1075, vol = 2)

julia> mixwort(SG(1080), 10, SG(1200), 5)
(sg = SG 1120, vol = 15)
```
"""
mixwort(sg1::SG, v1, sg2::SG, v2) = (sg = SG((sg1.val*v1+sg2.val*v2)/(v1+v2)), vol = v1+v2)

## grist composition

"""
    gristratio(maltweights)

For an array of weights, print the ratio of every malt.
Use the same unit, but only the value is needed.

# Examples
```julia-repl
julia> gristratio([3500, 500])
3500 --> 87.5%
500 --> 12.5%

julia> gristratio([7700, 1200])
7700 --> 86.5%
1200 --> 13.5%
```
"""
function gristratio(maltweights)
    s = sum(maltweights)
    for mw in maltweights
        println("$mw --> $(@sprintf("%.1f", mw/s*100))%")
    end
    return nothing
end

## measure diluted wort

"""
    mdwort(sg::SpecificGravity, vwort, vwater)

`mdwort` is a shorthand for measure diluted wort.
In the calculation only the ratio of the volumes of the liquids matter,
so it's sufficient to pass their ratio to the function.

# Arguments
- `sg::SpecificGravity`: specific gravity of the measured wort
- `vwort`: volume of the wort to be diluted.
- `vwater`: volume of water used for diluting.

# Examples

```julia-repl
julia> mdwort(SG(1025), 1, 2)
SG 1.0750

julia> mixwort(SG(1.075), 1, SG(1), 2) # check back
(sg = SG 1.0250, vol = 3)
```
"""
function mdwort(sg::SpecificGravity, vwort, vwater)
    SG((sg.val*(vwort+vwater)-vwater)/vwort)
end

## measure kettle volume

"""
    kettlevol(kd, kh, mh; unit=:cm)

Calculate the volume of wort in the kettle.
All arguments must have the same unit.
Returns the volume in m^3, and prints it in liters.

# Arguments
- `kd`: (inner) kettle diameter.
- `kh`: (inner) kettle height.
- `mh`: (inner) measured "height" of the wort: from the top of the kettle to the top of the wort.
- `unit=:cm`: can be: `mm`, `:cm`, `:dm`, `:m`.

# Examples
```julia-repl
julia> kettlevol(2.5, 3, 2, unit=:dm)
Wort volume: 4.91l
0.0049087385212340535
```
"""
function kettlevol(kd, kh, mh; unit=:cm)
    kd = measurement2metric(kd, unit)
    kh = measurement2metric(kh, unit)
    mh = measurement2metric(mh, unit)

    wortvol = (kd/2)^2*pi*(kh-mh)
    println("Wort volume: $(@sprintf("%.2f", wortvol*1000))l")
    return wortvol
end

"""
    measurement2metric(value, unit)

Convert a measured `value` in `unit` to metric unit (thus meters).
Supported units: `mm`, `:cm`, `:dm`, `:m`.
"""
function measurement2metric(value, unit)
    if unit === :m
        return value
    elseif unit === :dm
        return value*0.1
    elseif unit === :cm
        return value*0.01
    elseif unit === :mm
        return value*0.001
    else
        throw(DomainError(unit, "This unit is not supported."))
    end
end

## print available functions

"""
    info()

Print the available functions.

# Examples
```julia-repl
julia> info()
The following functions are available for use:
SG(1050) and setprintmode(0)
plato2sg(5.5)
sg2plato(SG(1050)) and sg2plato(1050)
gravitycorrection(SG(1050), SG(1025), 1)
mixwort(SG(1050), 1, SG(1000), 1)
gristratio([3500, 500])
mdwort(SG(1025), 1, 2)
kettlevol(2.5, 3, 2, unit=:dm)
dme2sugar(), sgvol2sugar(), raisesgbysugar() and sgofsugarliq()
info(): this help
```
"""
function info()
    println("The following functions are available for use:")
    println("SG(1050) and setprintmode(0)")
    println("plato2sg(5.5)")
    println("sg2plato(SG(1050)) and sg2plato(1050)")
    println("gravitycorrection(SG(1050), SG(1025), 1)")
    println("mixwort(SG(1050), 1, SG(1000), 1)")
    println("gristratio([3500, 500])")
    println("mdwort(SG(1025), 1, 2)")
    println("kettlevol(2.5, 3, 2, unit=:dm)")
    println("dme2sugar(), sgvol2sugar(), raisesgbysugar() and sgofsugarliq()")
    println("info(): this help")
    return nothing
end

## whacky functions to calculate:
## "how much sugar into water for given SG"

"""
dme2sugar(dme)

Calculate the "equivalent" of `dme` gramms of DME in gramms of sucrose.

# Examples

```julia
julia> dme2sugar(1)
0.9565217391304348

julia> 44/46
0.9565217391304348
```
"""
function dme2sugar(dme)
    # dme is 44ppg
    return dme*44/SUCROSEPPG
end


"""
    sgvol2sugar(sg::SG, vol)

How much sugar (sucrose) is needed to get `vol` liters
of liquid with `sg `gravity.
Result is in gramms.

# Examples

```julia-repl
julia> sgvol2sugar(SG(1384), 1) # sucrose pkl is 384, so this should be 1000
999.9999999999997
```
"""
function sgvol2sugar(sg::SG, vol)
    # sg and vol -> sucrose gramms
    targetpoints = 1000*(sg.val-1)
    return 1000*targetpoints/(SUCROSEPKL/vol)
end

"""
    raisesgbysugar(sgpoints, vol)

How much sugar (sucrose) is needed to raise the gravity of `vol` volume liquid
by `sgpoints` points.
Result is in gramms.

# Examples
```julia-repl
julia> raisesgbysugar(384, 1)
1000.0
```
"""
function raisesgbysugar(sgpoints, vol)
    return 1000*sgpoints/SUCROSEPKL*vol  
end


"""
sgofsugarliq(sugar, vol)

Calculate the SG of a liquid mixed from `vol` liters of water
and `sugar` gramms of sugar (sucrose).

# Examples
```julia
julia> sgofsugarliq(1000, 1)
SG 1384

julia> sgofsugarliq(1000, 0.5)
SG 1768
```
"""
function sgofsugarliq(sugar, vol)
    sgpoints = sugar/1000*SUCROSEPKL/vol
    return SG(1000+sgpoints)
end


"""
    totalsg(sg::SG, vol, sugarweight)

Calculate the total SG: after adding `sugarweight` gramms
of sucrose to `vol` liters of `sg` wort.

# Examples
```julia
julia> totalsg(SG(1093),9,354)
SG 1.1081
```
"""
function totalsg(sg::SG, vol, sugarweight)
    wort_sugar = sgvol2sugar(sg, vol)
    total_sugar = wort_sugar + sugarweight
    return sgofsugarliq(total_sugar, vol)
end