# process ispindel data from logfile

"""DATEFORMAT_DICT = Dict("default" => DateFormat("m/d/y, I:M:S p"), "v2" => DateFormat("yyyy. mm. dd. H:M:S"), "inkbird-v2" => DateFormat("yyyy. mm. dd. H:M:S"))"""
const DATEFORMAT_DICT = Dict("default" => DateFormat("m/d/y, I:M:S p"), "v2" => DateFormat("yyyy. mm. dd. H:M:S"), "inkbird-v2" => DateFormat("yyyy. mm. dd. H:M:S"),
"bpl-v1" => DateFormat("yyyy. mm. dd. H:M:S"), "bpl-v2" => DateFormat("yyyy. mm. dd. H:M:S"),  "bpl-v3" => DateFormat("yyyy. mm. dd. H:M:S"))

"""
    parsedate(date::AbstractString, formatname::AbstractString)

Construct `DateTime` from `date` based on `DATEFORMAT_DICT[formatname]`.
"""
function parsedate(date::AbstractString, formatname::AbstractString)
    return DateTime(date, DATEFORMAT_DICT[formatname])
end

"""
    parsedate(jsonline)

Parse `DateTime` from JSON line.
If the `formatversion` is specified, then use that from [`DATEFORMAT_DICT`](@ref),
use `default` otherwise.
"""
function parsedate(jsonline)
    # use default key if formatversion is not given
    dateformatkey = get(jsonline, "formatversion", "default")
    # use default key, if key is not found in DATEFORMAT_DICT
    dateformatkey = haskey(DATEFORMAT_DICT, dateformatkey) ? dateformatkey : "default"
    try
        date = parsedate(jsonline["time"], dateformatkey)
        return date
    catch e
        date = missing
        return date
    end
end

"""
    processlogfile(fname)

Process a vector of logs.
Return with a `NamedTuple` with keys: `"date", "sg",
"temperature", "angle", "battery", "plato"`.
Log entries without an "angle" field will be filtered.
"""
function processlog(log)
    lines = filter(x->haskey(x,"angle"), log)
    angle = [Float64(l["angle"]) for l in lines]
    temperature = [Float64(l["temperature"]) for l in lines]
    dates = parsedate.(lines)
    plato = [Float64(l["gravity"]) for l in lines]
    sg = [plato2sg(p).val for p in plato]
    battery = [Float64(l["battery"]) for l in lines]
    return (date=dates, sg=sg, temperature=temperature, angle=angle, battery=battery, plato=plato)
end

"""
    processlogfile(fname)

Process a logfile.
Return with a `NamedTuple` with keys: `"date", "sg",
"temperature", "angle", "battery", "plato"`.
Log entries without an "angle" field will be filtered.
"""
function processlogfile(fname; df::DateFormat=DateFormat("m/d/y, I:M:S p"))
    rawlines = readlines(fname) .|> JSON.parse
    return processlog(rawlines)
end


"""
    masg(dates, sg, stepsize)

Moving average of gravity.
A pretty stupid implementation.
"""
function masg(dates, sg, stepsize)
    @assert size(dates) == size(sg)
    nd = Vector{DateTime}(undef, 0)
    nsg = Vector{Float64}(undef, 0)

    i = 1
    while i < lastindex(dates)
        push!(nd, dates[i])
        ip = min(i+stepsize, lastindex(dates))
        push!(nsg, mean(sg[i:ip]))
        i = ip+1
    end
    return (nd, nsg)
end

"""
    promasg(logfiletuple::NamedTuple, window::Hour)

Moving average of gravity.
A different implementation, but I think still doesn't
comply with moving average's definition.
But it's smarter: goes through the data in `window`-sized steps
and calculates the mean of the `window` before every step.
"""
function promasg(logfiletuple::NamedTuple, window::TimePeriod)
    df = DataFrame("date"=>logfiletuple.date, "sg"=>logfiletuple.sg)
    daterange = collect(df[1, :date]:window:df[end, :date])
    sgs = Float64[]
    trash = Int[]
    for (i, d) in enumerate(daterange)
        qr = df |> @filter(d-window <= _.date <= d) |> DataFrame
        if isempty(qr)
            push!(trash, i)
            continue
        end
        push!(sgs, mean(qr[!,:sg]))
    end
    deleteat!(daterange, trash)
    return (daterange, sgs)
end

"""
    processtuyalog(log)

Expects an array of JSON object.

Process a Tuya-Inkbird logfile.
Return with a `NamedTuple` with keys: `"date",
"temperature", "set_temperature"`.
Log entries without an "cur_temp" field will be filtered.
"""
function processtuyalog(log)
    lines = filter(x->haskey(x,"cur_temp"), log)
    lines = filter(x->! (x["cur_temp"] =="None"), lines)
    temperature = parse.(Float64, [l["cur_temp"] for l in lines])
    dates = parsedate.(lines)
    set_temperature = parse.(Float64, [l["set_temp"] for l in lines])
    # post-process badly converted temperature values
    temperature = temperature ./ 10
    set_temperature = set_temperature ./ 5
    return (date=dates, temperature=temperature, set_temperature=set_temperature)
end

"""
    processtuyalogfile(fname)

Process a Tuya-Inkbird logfile.
Return with a `NamedTuple` with keys: `"date",
"temperature", "set_temperature"`.
Log entries without an "cur_temp" field will be filtered.
"""
function processtuyalogfile(fname)
    rawlines = readlines(fname) .|> JSON.parse
    return processtuyalog(rawlines)
end

"""
    toCET(d)

Convert UTC time to CET whacky mode.
"""
function toCET(d)
    return d+Hour(1)
end

const F64 = Float64

"""
    processbpllog(loglines::Vector{Dict{K,V}}) where {K,V}

Process brewpiless json log lines.
Return a `DataFrame`.
"""
function processbpllog(loglines::Vector{Dict{K,V}}, converttimestamp=true) where {K,V}
    df = DataFrame(id=String[], tempUnit=String[], beerTemp=F64[], beerSet=F64[],
    fridgeTemp=F64[], fridgeSet=F64[], roomTemp=F64[],
    gravity=F64[], tiltValue=F64[], auxTemp=F64[], extVolt=F64[], modeC=String[],
    timestamp=DateTime[], time=DateTime[], formatversion=String[])
    allowmissing!(df)
    for d in loglines
        haskey(d, "msg") && continue
        # "null" are read as nothing; here those are replaced with missing
        map!(x->isnothing(x) ? missing : x, values(d))
        
        ts = d["timestamp"] == 0 ? missing : unix2datetime(d["timestamp"])
        if converttimestamp
            ts = toCET(ts)
        end
        t = parsedate(d)
        mode = get(d, "modeC", missing)

        push!(df, [d["id"], d["tempUnit"], d["beerTemp"], d["beerSet"],
            d["fridgeTemp"], d["fridgeSet"], d["roomTemp"],
            d["gravity"], d["tiltValue"], d["auxTemp"], d["extVolt"], mode,
            ts, t, d["formatversion"]])
    end
    return df
end


function processbpllogfile(fname, converttimestamp=true)
    rawlines = JSON.parse.(readlines(fname))
    return processbpllog(rawlines, converttimestamp)
end

# beer temp: from ispindel
getbeertemp(df) = df[Not(ismissing.(df.beerTemp)), [:beerTemp, :timestamp, :time, :modeC]]
# beer set: from bpl
getbeerset(df) = df[Not(ismissing.(df.beerSet)), [:beerSet, :timestamp, :time, :modeC]]
# fridge temp and set: from bpl
getfridgetemp(df) = df[Not(ismissing.(df.fridgeTemp)), [:fridgeTemp, :fridgeSet, :timestamp, :time, :modeC]]
# ispndel: from ispindel
getispindeldata(df) = df[Not(ismissing.(df.beerTemp)), [:beerTemp, :auxTemp, :extVolt, :gravity, :tiltValue, :timestamp, :time, :modeC]]
