module BeerBrewHelpers

using JSON
using Dates: DateFormat, DateTime, TimePeriod, Hour, unix2datetime
using Printf: @sprintf
using Statistics: mean
using DataFrames, Query
using PrecompileTools

import Base.show

export  processlogfile,
        masg,
        promasg,
        processtuyalogfile,
        processbpllogfile,
        getbeertemp,
        getbeerset,
        getfridgetemp,
        getispindeldata

export  SpecificGravity,
        SG,
        setprintmode,
        sg2plato,
        plato2sg,
        gravitycorrection,
        mixwort,
        gristratio,
        mdwort,
        kettlevol,
        info,
        dme2sugar,
        sgvol2sugar,
        raisesgbysugar,
        sgofsugarliq,
        totalsg

include("ispindel.jl")
include("calculations.jl")

@setup_workload begin
    # Putting some things in `setup` can reduce the size of the
    # precompile file and potentially make loading faster.
    loglines = ["""{"name":"iSpindel010-cst","ID":10593204,"angle":64.06931,"temperature":21.6875,"temp_units":"C","battery":4.134515,"gravity":20.12155,"interval":900,"RSSI":-85,"time":"2022. 04. 17. 19:05:41","formatversion":"v2"}""",
    """{"name":"iSpindel010-cst","ID":10593204,"angle":64.07509,"temperature":21.125,"temp_units":"C","battery":4.103232,"gravity":20.12403,"interval":900,"RSSI":-89,"time":"2022. 04. 17. 19:20:00","formatversion":"v2"}""",
    """{"name":"iSpindel010-cst","ID":10593204,"angle":64.03986,"temperature":20.6875,"temp_units":"C","battery":4.139729,"gravity":20.10886,"interval":900,"RSSI":-88,"time":"2022. 04. 17. 19:48:46","formatversion":"v2"}""",
    """{"name":"iSpindel010-cst","ID":10593204,"angle":64.04923,"temperature":20.5625,"temp_units":"C","battery":4.134515,"gravity":20.1129,"interval":900,"RSSI":-89,"time":"2022. 04. 17. 20:03:03","formatversion":"v2"}""",
    """{"name":"iSpindel010-cst","ID":10593204,"angle":64.0592,"temperature":20.4375,"temp_units":"C","battery":4.139729,"gravity":20.11719,"interval":900,"RSSI":-91,"time":"2022. 04. 17. 20:17:26","formatversion":"v2"}""",
    """{"name":"iSpindel010-cst","ID":10593204,"angle":64.06645,"temperature":20.125,"temp_units":"C","battery":4.139729,"gravity":20.12031,"interval":900,"RSSI":-93,"time":"2022. 04. 17. 21:00:28","formatversion":"v2"}""",
    """{"name":"iSpindel010-cst","ID":10593204,"angle":64.05888,"temperature":20.0625,"temp_units":"C","battery":4.139729,"gravity":20.11705,"interval":900,"RSSI":-95,"time":"2022. 04. 17. 21:14:46","formatversion":"v2"}""",
    """{"name":"iSpindel010-cst","ID":10593204,"angle":64.10069,"temperature":20,"temp_units":"C","battery":4.139729,"gravity":20.13506,"interval":900,"RSSI":-91,"time":"2022. 04. 17. 21:29:09","formatversion":"v2"}""",
    """{"name":"iSpindel010-cst","ID":10593204,"angle":64.07367,"temperature":19.9375,"temp_units":"C","battery":4.129301,"gravity":20.12342,"interval":900,"RSSI":-92,"time":"2022. 04. 17. 21:43:33","formatversion":"v2"}""",
    """{"name":"iSpindel010-cst","ID":10593204,"angle":64.08068,"temperature":19.8125,"temp_units":"C","battery":4.129301,"gravity":20.12644,"interval":900,"RSSI":-92,"time":"2022. 04. 17. 21:57:56","formatversion":"v2"}""",
    """{"name":"iSpindel010-cst","ID":10593204,"angle":64.09203,"temperature":19.5625,"temp_units":"C","battery":4.108446,"gravity":20.13133,"interval":900,"RSSI":-93,"time":"2022. 04. 17. 22:12:16","formatversion":"v2"}""",
    """{"name":"iSpindel010-cst","ID":10593204,"angle":64.08533,"temperature":19.3125,"temp_units":"C","battery":4.129301,"gravity":20.12845,"interval":900,"RSSI":-94,"time":"2022. 04. 17. 22:26:40","formatversion":"v2"}""",
    """{"name":"iSpindel010-cst","ID":10593204,"angle":64.0955,"temperature":19.125,"temp_units":"C","battery":4.134515,"gravity":20.13282,"interval":900,"RSSI":-93,"time":"2022. 04. 17. 22:41:02","formatversion":"v2"}""",
    """{"name":"iSpindel010-cst","ID":10593204,"angle":64.13967,"temperature":18.875,"temp_units":"C","battery":4.134515,"gravity":20.15184,"interval":900,"RSSI":-91,"time":"2022. 04. 17. 22:55:23","formatversion":"v2"}""",
    """{"name":"iSpindel010-cst","ID":10593204,"angle":64.0854,"temperature":18.75,"temp_units":"C","battery":4.134515,"gravity":20.12847,"interval":900,"RSSI":-91,"time":"2022. 04. 17. 23:09:47","formatversion":"v2"}""",
    """{"name":"iSpindel010-cst","ID":10593204,"angle":64.11649,"temperature":18.5625,"temp_units":"C","battery":4.139729,"gravity":20.14186,"interval":900,"RSSI":-90,"time":"2022. 04. 17. 23:24:10","formatversion":"v2"}""",
    """{"name":"iSpindel010-cst","ID":10593204,"angle":64.09768,"temperature":18.5,"temp_units":"C","battery":4.134515,"gravity":20.13376,"interval":900,"RSSI":-91,"time":"2022. 04. 17. 23:38:33","formatversion":"v2"}""",
    """{"name":"iSpindel010-cst","ID":10593204,"angle":64.11659,"temperature":18.5,"temp_units":"C","battery":4.139729,"gravity":20.14191,"interval":900,"RSSI":-91,"time":"2022. 04. 17. 23:52:57","formatversion":"v2"}""",
    """{"name":"iSpindel010-cst","ID":10593204,"angle":64.08113,"temperature":18.4375,"temp_units":"C","battery":4.139729,"gravity":20.12664,"interval":900,"RSSI":-91,"time":"2022. 04. 18. 0:07:21","formatversion":"v2"}""",
    """{"name":"iSpindel010-cst","ID":10593204,"angle":64.09519,"temperature":18.4375,"temp_units":"C","battery":4.150157,"gravity":20.13269,"interval":900,"RSSI":-90,"time":"2022. 04. 18. 0:21:45","formatversion":"v2"}""",
    """{"name":"iSpindel010-cst","ID":10593204,"angle":64.11747,"temperature":18.4375,"temp_units":"C","battery":4.144943,"gravity":20.14228,"interval":900,"RSSI":-91,"time":"2022. 04. 18. 0:36:09","formatversion":"v2"}""",
    """{"name":"iSpindel010-cst","ID":10593204,"angle":64.11971,"temperature":18.4375,"temp_units":"C","battery":4.139729,"gravity":20.14325,"interval":900,"RSSI":-90,"time":"2022. 04. 18. 0:50:31","formatversion":"v2"}""",
    """{"name":"iSpindel010-cst","ID":10593204,"angle":64.10641,"temperature":18.375,"temp_units":"C","battery":4.139729,"gravity":20.13752,"interval":900,"RSSI":-89,"time":"2022. 04. 18. 1:04:56","formatversion":"v2"}""",
    """{"name":"iSpindel010-cst","ID":10593204,"angle":64.09782,"temperature":18.375,"temp_units":"C","battery":4.144943,"gravity":20.13382,"interval":900,"RSSI":-89,"time":"2022. 04. 18. 1:19:20","formatversion":"v2"}""",
    """{"name":"iSpindel010-cst","ID":10593204,"angle":64.07793,"temperature":18.3125,"temp_units":"C","battery":4.144943,"gravity":20.12526,"interval":900,"RSSI":-89,"time":"2022. 04. 18. 1:33:45","formatversion":"v2"}""",
    """{"name":"iSpindel010-cst","ID":10593204,"angle":64.0991,"temperature":18.3125,"temp_units":"C","battery":4.144943,"gravity":20.13437,"interval":900,"RSSI":-91,"time":"2022. 04. 18. 1:48:07","formatversion":"v2"}""",
    """{"name":"iSpindel010-cst","ID":10593204,"angle":64.089,"temperature":18.3125,"temp_units":"C","battery":4.144943,"gravity":20.13002,"interval":900,"RSSI":-89,"time":"2022. 04. 18. 2:02:31","formatversion":"v2"}"""]

    bplloglines = ["""{"id":"FERMENTER1","tempUnit":"C","beerTemp":null,"beerSet":null,"fridgeTemp":null,"fridgeSet":null,"roomTemp":null,"gravity":null,"tiltValue":0,"auxTemp":null,"extVolt":null,"timestamp":0,"time":"2023. 02. 06. 19:41:47","formatversion":"bpl-v1"}""",
    """{"id":"FERMENTER1","tempUnit":"C","beerTemp":null,"beerSet":null,"fridgeTemp":24.1,"fridgeSet":null,"roomTemp":null,"gravity":null,"tiltValue":0,"auxTemp":null,"extVolt":null,"timestamp":0,"time":"2023. 02. 06. 19:45:52","formatversion":"bpl-v1"}""",
    """{"id":"FERMENTER1","tempUnit":"C","beerTemp":null,"beerSet":null,"fridgeTemp":24.2,"fridgeSet":null,"roomTemp":null,"gravity":null,"tiltValue":0,"auxTemp":null,"extVolt":null,"timestamp":0,"time":"2023. 02. 06. 19:51:52","formatversion":"bpl-v1"}""",
    """{"id":"FERMENTER1","tempUnit":"C","beerTemp":null,"beerSet":null,"fridgeTemp":24.3,"fridgeSet":null,"roomTemp":null,"gravity":null,"tiltValue":0,"auxTemp":null,"extVolt":null,"timestamp":0,"time":"2023. 02. 06. 19:53:52","formatversion":"bpl-v1"}""",
    """{"id":"FERMENTER1","tempUnit":"C","beerTemp":null,"beerSet":null,"fridgeTemp":24.3,"fridgeSet":null,"roomTemp":null,"gravity":null,"tiltValue":0,"auxTemp":null,"extVolt":null,"timestamp":0,"time":"2023. 02. 06. 19:55:52","formatversion":"bpl-v1"}""",
    """{"id":"FERMENTER1","tempUnit":"C","beerTemp":null,"beerSet":null,"fridgeTemp":24.4,"fridgeSet":null,"roomTemp":null,"gravity":null,"tiltValue":0,"auxTemp":null,"extVolt":null,"timestamp":0,"time":"2023. 02. 06. 20:05:52","formatversion":"bpl-v1"}""",
    """{"id":"FERMENTER1","tempUnit":"C","beerTemp":39.9,"beerSet":null,"fridgeTemp":24.5,"fridgeSet":null,"roomTemp":null,"gravity":null,"tiltValue":66.75,"auxTemp":22.2,"extVolt":4,"timestamp":1675710436,"time":"2023. 02. 06. 20:07:52","formatversion":"bpl-v1"}""",
    """{"id":"FERMENTER1","tempUnit":"C","beerTemp":2.6,"beerSet":null,"fridgeTemp":24.6,"fridgeSet":null,"roomTemp":null,"gravity":null,"tiltValue":45.37,"auxTemp":22.9,"extVolt":4,"timestamp":1675710903,"time":"2023. 02. 06. 20:15:52","formatversion":"bpl-v1"}""",
    """{"id":"FERMENTER1","tempUnit":"C","beerTemp":99.4,"beerSet":null,"fridgeTemp":24.7,"fridgeSet":null,"roomTemp":null,"gravity":null,"tiltValue":45.38,"auxTemp":23,"extVolt":4,"timestamp":1675711059,"time":"2023. 02. 06. 20:17:52","formatversion":"bpl-v1"}""",
    """{"id":"FERMENTER1","tempUnit":"C","beerTemp":-14.7,"beerSet":null,"fridgeTemp":24.7,"fridgeSet":null,"roomTemp":null,"gravity":null,"tiltValue":45.38,"auxTemp":23,"extVolt":4,"timestamp":1675711059,"time":"2023. 02. 06. 20:19:52","formatversion":"bpl-v1"}""",
    """{"id":"FERMENTER1","tempUnit":"C","beerTemp":null,"beerSet":null,"fridgeTemp":24.7,"fridgeSet":null,"roomTemp":null,"gravity":null,"tiltValue":45.38,"auxTemp":23.1,"extVolt":4,"timestamp":1675711216,"time":"2023. 02. 06. 20:21:52","formatversion":"bpl-v1"}""",
    """{"id":"FERMENTER1","tempUnit":"C","beerTemp":null,"beerSet":null,"fridgeTemp":24.7,"fridgeSet":null,"roomTemp":null,"gravity":null,"tiltValue":0,"auxTemp":null,"extVolt":null,"timestamp":0,"time":"2023. 02. 06. 20:22:47","formatversion":"bpl-v1"}""",
    """{"id":"FERMENTER1","tempUnit":"C","beerTemp":null,"beerSet":null,"fridgeTemp":24.7,"fridgeSet":null,"roomTemp":null,"gravity":null,"tiltValue":0,"auxTemp":null,"extVolt":null,"timestamp":0,"time":"2023. 02. 06. 20:22:52","formatversion":"bpl-v1"}""",
    """{"id":"FERMENTER1","tempUnit":"C","beerTemp":52.7,"beerSet":null,"fridgeTemp":24.8,"fridgeSet":null,"roomTemp":null,"gravity":null,"tiltValue":45.49,"auxTemp":23.1,"extVolt":4,"timestamp":1675711373,"time":"2023. 02. 06. 20:24:52","formatversion":"bpl-v1"}""",
    """{"id":"FERMENTER1","tempUnit":"C","beerTemp":26.3,"beerSet":null,"fridgeTemp":24.8,"fridgeSet":null,"roomTemp":null,"gravity":null,"tiltValue":45.45,"auxTemp":23.2,"extVolt":4,"timestamp":1675711528,"time":"2023. 02. 06. 20:26:52","formatversion":"bpl-v1"}""",
    """{"msg":"brew ended","time":"2023. 02. 06. 20:32:33","formatversion":"bpl-v1"}""",
    """{"id":"FERMENTER1","tempUnit":"C","beerTemp":23.3,"beerSet":null,"fridgeTemp":24.9,"fridgeSet":null,"roomTemp":null,"gravity":null,"tiltValue":46.22,"auxTemp":23.3,"extVolt":4,"timestamp":1675711840,"time":"2023. 02. 06. 20:32:52","formatversion":"bpl-v1"}""",
    """{"id":"FERMENTER1","tempUnit":"C","beerTemp":23.3,"beerSet":null,"fridgeTemp":24.9,"fridgeSet":null,"roomTemp":null,"gravity":null,"tiltValue":46.22,"auxTemp":23.3,"extVolt":4,"timestamp":1675711840,"time":"2023. 02. 06. 20:34:52","formatversion":"bpl-v1"}""",
    """{"id":"FERMENTER1","tempUnit":"C","beerTemp":null,"beerSet":null,"fridgeTemp":24.7,"fridgeSet":null,"roomTemp":null,"gravity":null,"tiltValue":46.22,"auxTemp":23.3,"extVolt":4,"timestamp":1675711840,"time":"2023. 02. 06. 20:44:52","formatversion":"bpl-v1"}""",
    """{"id":"FERMENTER1","tempUnit":"C","beerTemp":9.19,"beerSet":null,"fridgeTemp":9.94,"fridgeSet":9,"roomTemp":null,"gravity":null,"tiltValue":88.4,"auxTemp":9.19,"extVolt":3.99,"timestamp":1676579595,"modeC":"f","time":"2023. 02. 16. 21:34:09","formatversion":"bpl-v2"}""",
    """{"id":"FERMENTER1","tempUnit":"C","beerTemp":9.13,"beerSet":null,"fridgeTemp":9.77,"fridgeSet":9,"roomTemp":null,"gravity":null,"tiltValue":88.41,"auxTemp":9.13,"extVolt":3.99,"timestamp":1676579751,"modeC":"f","time":"2023. 02. 16. 21:36:09","formatversion":"bpl-v2"}""",
    """{"id":"FERMENTER1","tempUnit":"C","beerTemp":9.13,"beerSet":null,"fridgeTemp":9.62,"fridgeSet":9,"roomTemp":null,"gravity":null,"tiltValue":88.41,"auxTemp":9.13,"extVolt":3.99,"timestamp":1676579751,"modeC":"f","time":"2023. 02. 16. 21:38:09","formatversion":"bpl-v2"}""",
    """{"id":"FERMENTER1","tempUnit":"C","beerTemp":9,"beerSet":null,"fridgeTemp":9.44,"fridgeSet":9,"roomTemp":null,"gravity":null,"tiltValue":88.38,"auxTemp":9,"extVolt":3.99,"timestamp":1676579907,"modeC":"f","time":"2023. 02. 16. 21:40:09","formatversion":"bpl-v2"}""",
    """{"id":"FERMENTER1","tempUnit":"C","beerTemp":8.94,"beerSet":null,"fridgeTemp":9.26,"fridgeSet":9,"roomTemp":null,"gravity":null,"tiltValue":88.39,"auxTemp":8.94,"extVolt":3.99,"timestamp":1676580064,"modeC":"f","time":"2023. 02. 16. 21:42:09","formatversion":"bpl-v2"}""",
    """{"id":"FERMENTER1","tempUnit":"C","beerTemp":8.81,"beerSet":null,"fridgeTemp":9.08,"fridgeSet":9,"roomTemp":null,"gravity":null,"tiltValue":88.41,"auxTemp":8.81,"extVolt":3.99,"timestamp":1676580219,"modeC":"f","time":"2023. 02. 16. 21:44:09","formatversion":"bpl-v2"}""",
    """{"id":"FERMENTER1","tempUnit":"C","beerTemp":8.81,"beerSet":null,"fridgeTemp":8.94,"fridgeSet":9,"roomTemp":null,"gravity":null,"tiltValue":88.41,"auxTemp":8.81,"extVolt":3.99,"timestamp":1676580219,"modeC":"f","time":"2023. 02. 16. 21:46:09","formatversion":"bpl-v2"}""",
    """{"id":"FERMENTER1","tempUnit":"C","beerTemp":8.81,"beerSet":null,"fridgeTemp":8.88,"fridgeSet":9,"roomTemp":null,"gravity":null,"tiltValue":88.41,"auxTemp":8.81,"extVolt":3.99,"timestamp":1676580375,"modeC":"f","time":"2023. 02. 16. 21:48:09","formatversion":"bpl-v2"}""",
    """{"id":"FERMENTER1","tempUnit":"C","beerTemp":8.69,"beerSet":null,"fridgeTemp":8.93,"fridgeSet":9,"roomTemp":null,"gravity":null,"tiltValue":88.39,"auxTemp":8.69,"extVolt":3.99,"timestamp":1676580531,"modeC":"f","time":"2023. 02. 16. 21:50:09","formatversion":"bpl-v2"}""",
    """{"id":"FERMENTER1","tempUnit":"C","beerTemp":8.63,"beerSet":null,"fridgeTemp":8.94,"fridgeSet":9,"roomTemp":null,"gravity":null,"tiltValue":88.4,"auxTemp":8.63,"extVolt":3.99,"timestamp":1676580687,"modeC":"f","time":"2023. 02. 16. 21:52:09","formatversion":"bpl-v2"}"""]

    @compile_workload begin
        # all calls in this block will be precompiled, regardless of whether
        # they belong to your package or not (on Julia 1.8 and higher)
        batch = processlog(JSON.parse.(loglines))
        pbd_ma16, pb_ma16 = promasg(batch, Hour(1))
        bpldf = processbpllog(JSON.parse.(bplloglines))
    end
end

end # module
