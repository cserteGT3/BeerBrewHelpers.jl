"""
    struct LabelSize

A structure to store labelsize related infos.
"""
struct LabelSize
    dpmm::Int64
    xmm::Int64
    ymm::Int64
    xp::Int64
    yp::Int64
end

"""
    LabelSize(dpi, xmm, ymm)

`dpi` will be converted to dpmm, so it's just an approximate DPI.
"""
function LabelSize(dpi, xmm, ymm)
    dpmm = ceil(dpi * 0.03937008)
    return LabelSize(dpmm, xmm, ymm, dpmm*xmm, dpmm*ymm)
end

"""
    mmLP(x, y, ls)

`mmLP` is shorthand for mm label point.
Calculates the position of the given points (in mm) for the given label (`ls`).
"""
function mmLP(x, y, ls)
    (0 <= x && x <= ls.xmm) || throw(DomainError(x, "x should be 0<=x<=ls.xmm"))
    (0 <= y && y <= ls.ymm) || throw(DomainError(y, "y should be 0<=y<=ls.ymm"))
    return Point(x*ls.dpmm, y*ls.dpmm)
end

"""
    aLP(x, y, ls)

`aLP` is shorthand for absolute label point.
Calculates the position of the given points (fom 0 to 1) for the given label (`ls`).
"""
function aLP(x, y, ls)
    (0 <= x && x <= 1) || throw(DomainError(x, "x should be 0<=x<=1"))
    (0 <= y && y <= 1) || throw(DomainError(y, "y should be 0<=y<=1"))
    return Point(x*ls.xp, y*ls.yp)
end

"""Vitals position."""
vitalpos(ls::LabelSize) = aLP(0.82, 0.72, ls)

"""Name pos."""
namepos(ls::LabelSize) = aLP(0.5, 0.24, ls)

function vitals(ibu, balling, abv, ls)
    pos = vitalpos(ls)
    fontsize(32)
    h_alignment = :center
    v_alignment = :middle
    text("IBU: $ibu", pos-aLP(0, 0.04, ls), halign=h_alignment, valign=v_alignment)
    text("BALLING: $(balling)°", pos, halign=h_alignment , valign=v_alignment)
    text("ABV: $abv%", pos+aLP(0, 0.04, ls), halign=h_alignment, valign=v_alignment)
end

function nametag(ls::LabelSize, batchnum, bottleday)
    pos = vitalpos(ls) - aLP(0.53, 0, ls)
    fontsize(32)
    h_alignment = :center
    v_alignment = :middle
    text("Batch: No.$batchnum", pos-aLP(0, 0.04, ls), halign=h_alignment, valign=v_alignment)
    text("Palackozva: $bottleday", pos, halign=h_alignment, valign=v_alignment)
    text("Sörfőző: Cserteg Tamás", pos+aLP(0, 0.04, ls), halign=h_alignment, valign=v_alignment)
    #=
    text("Batch: No.$batchnum", tagpos+mmLP(0, 2.8, ls), halign=:left)
    text("@cserteGT3", halign=:center, valign = :bottom)
    @layer begin
        scale(0.1)
        julialogo()
    end
    =#
    origin()
end

function beername(name, style, ls)
    h_alignment = :center
    fontsize(92)
    text(name, namepos(ls), halign=h_alignment)
    fontsize(54)
    text(style, namepos(ls)+aLP(0, 0.114, ls), halign=h_alignment)
end

function label_no10(label_name)
    ls = LabelSize(300, 92, 70)
    Drawing(ls.xp, ls.yp, label_name)
    # background
    background("white")
    
    ## text
    fontface("JuliaMono")
    beername("Legjobb Rózsika", "Best Bitter", ls)
    vitals(30, 11, 4.5, ls)
    nametag(ls, 10, "2021.03.10.")
    finish()
    preview()
end


function label_no11(label_name)
    ls = LabelSize(300, 92, 70)
    ## background
    Drawing(ls.xp, ls.yp, label_name)
    setmode("source")
    gray2gray = blend(Point(ls.xp/2, 0), Point(ls.xp/2, ls.yp), "grey12", "grey2")
    setblend(gray2gray)
    box(Point(0, 0), Point(ls.xp, ls.yp), :fill)
    #background("gray")

    ## text
    fontface("JuliaMono")
    setcolor("grey65")
    beername("RIS 60", "Imperial Stout", ls)
    vitals(107, 22, 9.8, ls)
    nametag(ls, 11, "2021.03.26.")
    finish()
    preview()
end
